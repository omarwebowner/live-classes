import React, { useState, useEffect, useRef } from 'react';
import { Theme, FormControl, Tooltip } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import Button from '../components/custom-button';
import FormInput from '../components/form-input';
import LangSelect from '../components/lang-select';
import { isElectron } from '../utils/platform';
import { usePlatform } from '../containers/platform-container';
import {useHistory} from 'react-router-dom';
import { roomStore } from '../stores/room';
import { globalStore, roomTypes } from '../stores/global';
import { t } from '../i18n';
import GlobalStorage from '../utils/custom-storage';
import { genUUID } from '../utils/api';
import Log from '../utils/LogUploader';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
let Verified = 'false';
let SessSecured = 'false';

var bcrypt = require('bcryptjs');

const useStyles = makeStyles ((theme: Theme) => ({
  formControl: {
    minWidth: '240px',
    maxWidth: '240px',
  }
}));

type SessionInfo = {
  roomName: string
  roomType: number
  yourName: string
  role: string
  password: string

}

const defaultState: SessionInfo = {
  roomName: '',
  roomType: 0,
  role: '',
  yourName: '',
  password: ''

}
const parsed = queryString.parse(location.search);
var RoomType = Number(parsed.RoomType);
var roomName = parsed.roomName as string;
var UserName = parsed.UserName as string;
var Role = parsed.Role as string;
let hash = parsed.hash as string;
let secured = parsed.secure as string;

if(hash != undefined && hash != null ){
  hash = hash.replace(/^\$2y(.+)$/i, '$2a$1');

}

if(secured != undefined && secured != null ){
  secured = secured.replace(/^\$2y(.+)$/i, '$2a$1');

}
var ValidSecure = `${RoomType+roomName+UserName+Role}` ;



function HomePage() {
  document.title = t(`home.short_title.title`)

  const classes = useStyles();

  const history = useHistory();

  const handleSetting = (evt: any) => {
    history.push({pathname: `/device_test`});
  }

  const [lock, setLock] = useState<boolean>(false);

  const handleUpload = (evt: any) => {
    setLock(true)
    Log.doUpload().then((resultCode: any) => {
      globalStore.showDialog({
        type: 'uploadLog',
        message: t('toast.show_log_id', {reason: `${resultCode}`})
      });
    }).finally(() => {
      setLock(false)
    })
  }

  const {
    HomeBtn
  } = usePlatform();

  const ref = useRef<boolean>(false);

  useEffect(() => {
    return () => {
      ref.current = true;
    }
  }, []);

  const [session, setSessionInfo] = useState<SessionInfo>(defaultState);

  const [required, setRequired] = useState<any>({} as any);
  session.roomName = roomName as string;
  session.yourName = UserName as string; 
  session.role = Role as string;
  session.roomType = RoomType;

  const isVerified = bcrypt.compare(session.password, hash, (err: any, res: any) => {
if(res){
  Verified = 'true';
}else{
  Verified = 'false';
}
  });


  const isSecured = bcrypt.compare(ValidSecure, secured, (err: any, res: any) => {
    if(res){
      SessSecured = 'true';
    }else{
      SessSecured = 'false';
    }
      });

  const handleSubmit = () => {
    if (!session.password) {
      setRequired({...required, password: t('home.password')});
      return;
    }

    if (Verified != 'true') {
      globalStore.showToast({
        type: 'rtmClient',
        message:'Join Class Failed: Wrong Password!',
      })
      return;
    }
    if(SessSecured != 'true'){
      globalStore.showToast({
        type: 'rtmClient',
        message:'Fraud detected!',
      })
      return;
    }
    
    if (!roomTypes[session.roomType]) return;
    const path = roomTypes[session.roomType].path
    globalStore.showLoading()
    roomStore.LoginToRoom({
      userName: session.yourName,
      roomName: session.roomName,
      role: session.role === 'teacher' ? 1 : 2,
      type: session.roomType,
      uuid: genUUID()
    }).then(() => {
      history.push(`/classroom/${path}`)
    }).catch((err: any) => {
      if (err.hasOwnProperty('api_error')) {
        return
      }
      if (err.reason) {
        globalStore.showToast({
          type: 'rtmClient',
          message: t('toast.rtm_login_failed_reason', {reason: err.reason}),
        })
      } else {
        globalStore.showToast({
          type: 'rtmClient',
          message: t('toast.rtm_login_failed'),
        })
      }
      console.warn(err)
    }).finally(() => {
      globalStore.stopLoading();
    })
  } 

 
  return (
    <div className={`flex-container ${isElectron ? 'draggable' : 'home-cover-web' }`}>
     
      <div className="custom-card">
        <div className="flex-item cover">
          {isElectron ? 
          <>
          <div className={`short-title ${globalStore.state.language}`}>
            <span className="title">{t('home.short_title.title')}</span>
            <span className="subtitle">{t('home.short_title.subtitle')}</span>
          </div>
          <div className={`cover-placeholder ${t('home.cover_class')}`}></div>
          <div className='build-version'>{t("build_version")}</div>
          </>
          : <div className={`cover-placeholder-web ${t('home.cover_class')}`}></div>
          }
        </div>
        <div className="flex-item card">
          <div className="position-top card-menu">
            <HomeBtn handleSetting={handleSetting}/>
          </div>
          <div className="position-content flex-direction-column">
            


            <FormControl   className={classes.formControl}>
              <FormInput type="password" Label={t('home.password')} value={session.password} onChange={
                (val: string) => {
                  setSessionInfo({
                    ...session,
                    password: val
                  });
                }}
                requiredText={required.password}
              />
            </FormControl>
           
           
           
            <Button name={t('home.room_join')} onClick={handleSubmit}/>
          </div>
        </div>
      </div>
    </div>
  )
}
export default React.memo(HomePage);